import 'package:http_client/browser.dart';


Future<String> getHash(String page) async {
  final client = BrowserClient();
  final rs = await client.send(Request('GET', '/api/v1/get/hash/${page}'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}
Future<String> chkHash(String page, hash) async {
  final client = BrowserClient();
  final rs = await client.send(Request('GET', '/api/v1/chk/${hash}/${page}'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}
Future<String> recHashActionPage(String page, hash, action) async {
  final client = BrowserClient();
  final rs = await client.send(Request('GET', '/api/v1/rec/${hash}/${action}/${page}'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}
Future<String> salHash(String hash) async {
  final client = BrowserClient();
  final rs = await client.send(Request('GET', '/api/v1/sal/${hash}'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}

Future<String> dataLeadPage() async {
  final client = new BrowserClient();
  final rs = await client.send(new Request('GET', '/static/data/message.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}
Future<String> dataFooter() async {
  final client = new BrowserClient();
  final rs = await client.send(new Request('GET', '/static/data/footer.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}

Future<String> dataMenu() async {
  final client = new BrowserClient();
  final rs = await client.send(new Request('GET', '/static/data/menu.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}

Future<String> dataLogo() async {
  final client = new BrowserClient();
  final rs = await client.send(new Request('GET', '/static/data/logo.json'));
  final textContent = await rs.readAsString();
  await client.close();
  return textContent;
}